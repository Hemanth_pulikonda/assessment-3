
import java.util.Scanner;

public class Question4 
{
	public static void main(String args[]) 
    {
        int sum= 0,pro=1,num;
        
        Scanner s = new Scanner(System.in);
        
        System.out.println("Enter the number of array elements : ");
        
        num = s.nextInt();
        
        int [] a = new int[num];
        
        System.out.println("Enter the array elements :  ");
        
        for (int i = 0; i < num; i++)
        {
            System.out.println("Enter the "+ num + " elements:");
            a[i] = s.nextInt();
        }
        
        for (int i = 0; i < num; i++)
        {
            sum = sum + a[i];
            pro = pro * a[i];
        }
        System.out.println("Sum of array elements is: "+sum);
        System.out.println("Product of array elements is: "+pro);
        
    }


}
